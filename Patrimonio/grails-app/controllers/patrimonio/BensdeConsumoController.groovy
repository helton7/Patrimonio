package patrimonio



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class BensdeConsumoController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond BensdeConsumo.list(params), model:[bensdeConsumoInstanceCount: BensdeConsumo.count()]
    }

    def show(BensdeConsumo bensdeConsumoInstance) {
        respond bensdeConsumoInstance
    }

    def create() {
        respond new BensdeConsumo(params)
    }

    @Transactional
    def save(BensdeConsumo bensdeConsumoInstance) {
        if (bensdeConsumoInstance == null) {
            notFound()
            return
        }

        if (bensdeConsumoInstance.hasErrors()) {
            respond bensdeConsumoInstance.errors, view:'create'
            return
        }

        bensdeConsumoInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'bensdeConsumo.label', default: 'BensdeConsumo'), bensdeConsumoInstance.id])
                redirect bensdeConsumoInstance
            }
            '*' { respond bensdeConsumoInstance, [status: CREATED] }
        }
    }

    def edit(BensdeConsumo bensdeConsumoInstance) {
        respond bensdeConsumoInstance
    }

    @Transactional
    def update(BensdeConsumo bensdeConsumoInstance) {
        if (bensdeConsumoInstance == null) {
            notFound()
            return
        }

        if (bensdeConsumoInstance.hasErrors()) {
            respond bensdeConsumoInstance.errors, view:'edit'
            return
        }

        bensdeConsumoInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'BensdeConsumo.label', default: 'BensdeConsumo'), bensdeConsumoInstance.id])
                redirect bensdeConsumoInstance
            }
            '*'{ respond bensdeConsumoInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(BensdeConsumo bensdeConsumoInstance) {

        if (bensdeConsumoInstance == null) {
            notFound()
            return
        }

        bensdeConsumoInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'BensdeConsumo.label', default: 'BensdeConsumo'), bensdeConsumoInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'bensdeConsumo.label', default: 'BensdeConsumo'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
