package patrimonio



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class BensPermanentesController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond BensPermanentes.list(params), model:[bensPermanentesInstanceCount: BensPermanentes.count()]
    }

    def show(BensPermanentes bensPermanentesInstance) {
        respond bensPermanentesInstance
    }

    def create() {
        respond new BensPermanentes(params)
    }

    @Transactional
    def save(BensPermanentes bensPermanentesInstance) {
        if (bensPermanentesInstance == null) {
            notFound()
            return
        }

        if (bensPermanentesInstance.hasErrors()) {
            respond bensPermanentesInstance.errors, view:'create'
            return
        }

        bensPermanentesInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'bensPermanentes.label', default: 'BensPermanentes'), bensPermanentesInstance.id])
                redirect bensPermanentesInstance
            }
            '*' { respond bensPermanentesInstance, [status: CREATED] }
        }
    }

    def edit(BensPermanentes bensPermanentesInstance) {
        respond bensPermanentesInstance
    }

    @Transactional
    def update(BensPermanentes bensPermanentesInstance) {
        if (bensPermanentesInstance == null) {
            notFound()
            return
        }

        if (bensPermanentesInstance.hasErrors()) {
            respond bensPermanentesInstance.errors, view:'edit'
            return
        }

        bensPermanentesInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'BensPermanentes.label', default: 'BensPermanentes'), bensPermanentesInstance.id])
                redirect bensPermanentesInstance
            }
            '*'{ respond bensPermanentesInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(BensPermanentes bensPermanentesInstance) {

        if (bensPermanentesInstance == null) {
            notFound()
            return
        }

        bensPermanentesInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'BensPermanentes.label', default: 'BensPermanentes'), bensPermanentesInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'bensPermanentes.label', default: 'BensPermanentes'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
