package patrimonio



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class RelatorioController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Relatorio.list(params), model:[relatorioInstanceCount: Relatorio.count()]
    }

    def show(Relatorio relatorioInstance) {
        respond relatorioInstance
    }

    def create() {
        respond new Relatorio(params)
    }

    @Transactional
    def save(Relatorio relatorioInstance) {
        if (relatorioInstance == null) {
            notFound()
            return
        }

        if (relatorioInstance.hasErrors()) {
            respond relatorioInstance.errors, view:'create'
            return
        }

        relatorioInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'relatorio.label', default: 'Relatorio'), relatorioInstance.id])
                redirect relatorioInstance
            }
            '*' { respond relatorioInstance, [status: CREATED] }
        }
    }

    def edit(Relatorio relatorioInstance) {
        respond relatorioInstance
    }

    @Transactional
    def update(Relatorio relatorioInstance) {
        if (relatorioInstance == null) {
            notFound()
            return
        }

        if (relatorioInstance.hasErrors()) {
            respond relatorioInstance.errors, view:'edit'
            return
        }

        relatorioInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Relatorio.label', default: 'Relatorio'), relatorioInstance.id])
                redirect relatorioInstance
            }
            '*'{ respond relatorioInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Relatorio relatorioInstance) {

        if (relatorioInstance == null) {
            notFound()
            return
        }

        relatorioInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Relatorio.label', default: 'Relatorio'), relatorioInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'relatorio.label', default: 'Relatorio'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
