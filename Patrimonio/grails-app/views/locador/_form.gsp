<%@ page import="patrimonio.Locador" %>



<div class="fieldcontain ${hasErrors(bean: locadorInstance, field: 'email', 'error')} required">
	<label for="email">
		<g:message code="locador.email.label" default="Email" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="email" name="email" required="" value="${locadorInstance?.email}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: locadorInstance, field: 'bairro', 'error')} required">
	<label for="bairro">
		<g:message code="locador.bairro.label" default="Bairro" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="bairro" required="" value="${locadorInstance?.bairro}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: locadorInstance, field: 'fone', 'error')} required">
	<label for="fone">
		<g:message code="locador.fone.label" default="Fone" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="fone" required="" value="${locadorInstance?.fone}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: locadorInstance, field: 'nome', 'error')} required">
	<label for="nome">
		<g:message code="locador.nome.label" default="Nome" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nome" required="" value="${locadorInstance?.nome}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: locadorInstance, field: 'numero', 'error')} required">
	<label for="numero">
		<g:message code="locador.numero.label" default="Numero" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="numero" required="" value="${locadorInstance?.numero}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: locadorInstance, field: 'rua', 'error')} required">
	<label for="rua">
		<g:message code="locador.rua.label" default="Rua" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="rua" required="" value="${locadorInstance?.rua}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: locadorInstance, field: 'sexo', 'error')} required">
	<label for="sexo">
		<g:message code="locador.sexo.label" default="Sexo" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="sexo" required="" value="${locadorInstance?.sexo}"/>

</div>

