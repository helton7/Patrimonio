<%@ page import="patrimonio.BensdeConsumo" %>



<div class="fieldcontain ${hasErrors(bean: bensdeConsumoInstance, field: 'descricao', 'error')} required">
	<label for="descricao">
		<g:message code="bensdeConsumo.descricao.label" default="Descricao" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="descricao" required="" value="${bensdeConsumoInstance?.descricao}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: bensdeConsumoInstance, field: 'foto', 'error')} required">
	<label for="foto">
		<g:message code="bensdeConsumo.foto.label" default="Foto" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="foto" required="" value="${bensdeConsumoInstance?.foto}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: bensdeConsumoInstance, field: 'quantidade', 'error')} required">
	<label for="quantidade">
		<g:message code="bensdeConsumo.quantidade.label" default="Quantidade" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="quantidade" required="" value="${bensdeConsumoInstance?.quantidade}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: bensdeConsumoInstance, field: 'tombamento_bc', 'error')} required">
	<label for="tombamento_bc">
		<g:message code="bensdeConsumo.tombamento_bc.label" default="Tombamentobc" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="tombamento_bc" type="number" value="${bensdeConsumoInstance.tombamento_bc}" required=""/>

</div>

