
<%@ page import="patrimonio.BensdeConsumo" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'bensdeConsumo.label', default: 'BensdeConsumo')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-bensdeConsumo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-bensdeConsumo" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="descricao" title="${message(code: 'bensdeConsumo.descricao.label', default: 'Descricao')}" />
					
						<g:sortableColumn property="foto" title="${message(code: 'bensdeConsumo.foto.label', default: 'Foto')}" />
					
						<g:sortableColumn property="quantidade" title="${message(code: 'bensdeConsumo.quantidade.label', default: 'Quantidade')}" />
					
						<g:sortableColumn property="tombamento_bc" title="${message(code: 'bensdeConsumo.tombamento_bc.label', default: 'Tombamentobc')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${bensdeConsumoInstanceList}" status="i" var="bensdeConsumoInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${bensdeConsumoInstance.id}">${fieldValue(bean: bensdeConsumoInstance, field: "descricao")}</g:link></td>
					
						<td>${fieldValue(bean: bensdeConsumoInstance, field: "foto")}</td>
					
						<td>${fieldValue(bean: bensdeConsumoInstance, field: "quantidade")}</td>
					
						<td>${fieldValue(bean: bensdeConsumoInstance, field: "tombamento_bc")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${bensdeConsumoInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
